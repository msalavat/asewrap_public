#!/bin/bash
#==++++++++++++++++++++++++++++++++++++++++++++++++++++++++==#
#==                    SGE ASE job wrapper                 ==#
#==                            by                          ==#
#==                   Dr. Mazdak Salavati                  ==#
#==                        2017 - 2019                     ==#
#==++++++++++++++++++++++++++++++++++++++++++++++++++++++++==#


printf "This wrapper takes in the user input as job name and if the job name is matching
the subscripts prefix [e.g. format EXP0001 3chars+4nums] it will submit them to the scheduler in order.
Place all the scripts in the following address: $HOME/scripts/
This wrapper is ONLY tested under following machine setup: 
Linux 3.10.0-327.36.3.el7.x86_64
Sungrid Engine:  sge/2011.11p1_155"

echo "Input the JOBNAME e.g. EXP0001 followed by [ENTER]:"
read JOB

if [ -e $HOME/scripts/${JOB}_AN1.sh ];
	then
		qsub $HOME/scripts/"$JOB"_AN1.sh && \
		qsub -hold_jid "$JOB"_AN1 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_AN2.sh && \
		qsub -hold_jid "$JOB"_AN2 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_AN3.sh && \
		qsub -hold_jid "$JOB"_AN3 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_AN3_1.sh && \
		qsub -hold_jid "$JOB"_AN4 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_AN5.sh && \
		qsub -hold_jid "$JOB"_AN5 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_AN6.sh && \
		qsub -hold_jid "$JOB"_AN6 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_AN7.sh && \
		qsub -hold_jid "$JOB"_AN7 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_AN8.sh && \
		qsub -hold_jid "$JOB"_AN8 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_R1.sh && \
		qsub -hold_jid "$JOB"_R1 -v JOBNAME=$JOBNAME $HOME/scripts/"$JOB"_R2.sh && \
	else
		echo "The script file doesn't exist in the -- $HOME/scripts --!"
fi;

echo "$JOB has been submitted for ASE pipeline and Static GeneiASE model"