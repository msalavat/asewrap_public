#!/bin/bash

#SGE flags
#$ -N EXP0000_AN4													##TBC
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=30G
#$ -r yes
#$ -notify 
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V
#$ -t 1																##TBC The number of tasks should be set by the number of input BAM files in each run 


#Abs paths
REF=$HOME/workplace
REF2=$HOME/workplace/Ovis_aries3.1
SOURCE=$HOME/bams
mkdir -p $HOME/EXP0000														##TBC
WASP=$HOME/wasp
DESTINATION=$HOME/EXP0000													##TBC

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/EXP0000_output										##TBC
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=$HOME/EXP0000/bams													##TBC
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/EXP0000_output											##TBC

#Loading modules
module load fastqc/0.11.7
module load trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load samtools/1.6
module load picard/2.17.11
module load vcftools/0.1.13
module load bcftools/1.6
module load python/2.7.13


#Remapping loop (REF sorted and Indexed)
cd $OUTPUT

ls -1 ./WASP/*/*_rehead.bam > array.list_$SGE_TASK_ID
infile=$(awk "NR==$SGE_TASK_ID" array.list_$SGE_TASK_ID)
BAMNAME=$(basename -s _rehead.bam $infile)

#Ref sort
picardtools \
ReorderSam \
REFERENCE=${REF2}/Oar_v3.1.92.fa \
QUIET=true \
I=${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped.bam \
O=${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped_sorted.bam \
TMP_DIR=$TMP
	
# BuildBamIndex
picardtools \
BuildBamIndex \
QUIET=true \
I=${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped_sorted.bam \
O=${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped_sorted.bam.bai

rm -f ./array.list_*