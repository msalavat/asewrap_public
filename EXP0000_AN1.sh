#!/bin/bash

#SGE flags
#$ -N EXP0000_AN1															##TBC
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=20G
#$ -r yes
#$ -notify 
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 4
#$ -V

#Abs paths
REF=$HOME/workplace
REF2=$HOME/workplace/Ovis_aries3.1
SOURCE=$HOME/bams
mkdir -p $HOME/EXP0000														##TBC
WASP=$HOME/wasp
DESTINATION=$HOME/EXP0000													##TBC

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/EXP0000_output										##TBC
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=$HOME/EXP0000/bams													##TBC
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/EXP0000_output											##TBC

#Loading modules
module load fastqc/0.11.7
module load trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load samtools/1.6
module load picard/2.17.11
module load vcftools/0.1.13
module load bcftools/1.6
module load python/2.7.13

#WASP pre-loop
cd $OUTPUT
mkdir -p ${OUTPUT}/WASP

for b in ${INPUT}/*/*.bam;do												##TBC Name of the sample/folder containing the BAM file
	
	BAMNAME=$(basename -s .bam $b)
	
	#Original output forlder
	mkdir -p ${OUTPUT}/WASP/${BAMNAME}

	#SNP file for WASP requires special treatment
	samtools view -H $b > tmp_header.sam
	sed -i -e '/SN:[0-9]/ s/SN:/SN:chr/' tmp_header.sam 
	samtools reheader tmp_header.sam $b > ${OUTPUT}/WASP/$BAMNAME/${BAMNAME}_rehead.bam
	
	#Synthesis (this folder name will be fed to array job by $(basename -s _rehead.bam $infile) in AN2 script
	mkdir -p ${OUTPUT}/WASP/${BAMNAME}/synthetic
	
done; 