# asewrap_public

This is the wrapped pipeline for the publication titled: 

"Elimination of reference mapping bias reveals robust immune related allele-specific expression in cross-bred sheep"

Mazdak Salavati 1*, Stephen J. Bush 1, Sergio Palma-Vera 2, Mary E.B. McCulloch 1, David A. Hume 3 and Emily L. Clark 1*

1.	The Roslin Institute and Royal (Dick) School of Veterinary Studies, University of Edinburgh, Easter Bush, Midlothian, UK
2.	Leibniz Institute for Farm Animal Biology (FBN), Dummerstorf, Germany
3.	Mater Research Institute-University of Queensland, Translational Research Institute, Woolloongabba, Qld 4102, Australia
* Correspondence: 	
Emily.Clark@roslin.ed.ac.uk 
Mazdak.Salavati@roslin.ed.ac.uk 

DOI:

BioRxiv DOI: https://doi.org/10.1101/619122
Data availability: https://www.ebi.ac.uk/ena/data/view/PRJEB19199
The BAM files were already produced as part of the sheep gene expression atlas following the publicly available protocol (FAANG, 2018) and as described in (Clark et al. 2017). The BAM files have been
uploaded to ENA under accession numbers ERZ827944, ERZ827949, ERZ827951, ERZ827955, ERZ827972, ERZ827988, ERZ827995, ERZ827997, ERZ828001, ERZ828016, ERZ828019, ERZ828036, ERZ828044, ERZ828046, ERZ828050, ERZ828070, ERZ828073, ERZ828160, ERZ828167, ERZ828168, ERZ828172, ERZ828188, ERZ828192, ERZ828209, ERZ828215, ERZ828217, ERZ828221, ERZ828240, ERZ828244, ERZ828261, ERZ828268, ERZ828270, ERZ828274, ERZ828293 and ERZ828297. 




