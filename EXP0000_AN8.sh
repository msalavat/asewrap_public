#!/bin/bash

#SGE flags
#$ -N EXP0000_AN8												##TBC
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=20G
#$ -r yes
#$ -notify 
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 4
#$ -V

#Abs paths
REF=$HOME/workplace
REF2=$HOME/workplace/Ovis_aries3.1
SOURCE=$HOME/bams
mkdir -p $HOME/EXP0000														##TBC
WASP=$HOME/wasp
DESTINATION=$HOME/EXP0000													##TBC

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/EXP0000_output										##TBC
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=$HOME/EXP0000/bams													##TBC
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/EXP0000_output											##TBC

#Loading modules
module load fastqc/0.11.7
module load trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load samtools/1.6
module load picard/2.17.11
module load vcftools/0.1.13
module load bcftools/1.6
module load python/2.7.13

cd ${OUTPUT}

####Sample file name for RG manipulation

for f in ${OUTPUT}/WASP/*/*_ASE_ready_SO_rmdup.bam;do
	FILENAME=$(basename -s _ASE_ready_SO_rmdup.bam $f)
	
	#Brining the ENSEMBL terminology back for GATK
	samtools view -H $f > tmp_header.sam
	sed -i -e '/SN:chr/ s/SN:chr/SN:/' tmp_header.sam 
	samtools reheader tmp_header.sam $f > tmp.bam
	
	picardtools \
	ReorderSam \
	REFERENCE=${REF2}/Oar_v3.1.92.fa \
	QUIET=true \
	I=tmp.bam \
	O=tmp2.bam \
	TMP_DIR=$TMP

	#Name of the BAM file might change the column in the <<< syntax
	picardtools \
	AddOrReplaceReadGroups QUIET=true I=tmp2.bam O=tmp3.bam TMP_DIR=$TMP \
	RGID=$(cut -d'_' -f1 <<< "${FILENAME}") \
	RGSM=$(cut -d'_' -f2 <<< "${FILENAME}") \
	RGPL=illumina \
	RGLB=HD_frstrand \
	RGPU=C7HU5ANXX
	
	mv -f tmp3.bam $f	
	
	# BuildBamIndex
	picardtools \
	BuildBamIndex \
	QUIET=true \
	I=$f \
	O=$f.bai

	#cleanup
	rm -f tmp_header.sam tmp.bam tmp2.bam tmp3.bam
done;


mkdir -p ${OUTPUT}/ASE/

for a in ${OUTPUT}/WASP/*/*_ASE_ready_SO_rmdup.bam;do 
	VCFNAME=$(basename -s _ASE_ready_SO_rmdup.bam $a)

	mkdir -p ${OUTPUT}/ASE/${VCFNAME}/
	mkdir -p ${OUTPUT}/ASE/${VCFNAME}/ASE_Count/

	java -jar ${HOME}/tools/GenomeAnalysisTK.jar -T ASEReadCounter \
	-R ${REF2}/Oar_v3.1.92.fa \
	-sites ${REF2}/Oar_v3.1.92_SNV.vcf.gz \
	-I $a \
	-o ${OUTPUT}/ASE/${VCFNAME}/ASE_Count/${VCFNAME}_ASERC.csv \
	-U ALLOW_N_CIGAR_READS \
	--outputFormat CSV \
	-mmq 50 \
	-mbq 25
done;
