#!/bin/bash

#SGE flags
#$ -N EXP0000_R3													##TBC
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=30G
#$ -R yes
#$ -r yes
#$ -notify 
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V

#Abs paths
REF=$HOME/workplace
REF2=$HOME/workplace/Ovis_aries3.1
SOURCE=$HOME/bams
mkdir -p $HOME/EXP0000														##TBC
WASP=$HOME/wasp
DESTINATION=$HOME/EXP0000													##TBC

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/EXP0000_output										##TBC
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=$HOME/EXP0000/bams													##TBC
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/EXP0000_output											##TBC

#Loading module
module load R/3.4.3

#the prep scripts should be tailored to each experiment (specially the ICD mode)
mkdir -p ${OUTPUT}/GeneiASE_SVP/output_icd_default/
R CMD BATCH --no-save $HOME/Rscripts/R_prep_icd.R $HOME/logs/EXP0000_R_prep_icd.Rout			

