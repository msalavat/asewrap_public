#!/bin/bash
#This is the script for the analysis 

#SGE flags
#$ -N EXP0000_R1										##TBC
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=30G
#$ -R yes
#$ -r yes
#$ -notify 
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V

#Abs paths
REF=$HOME/workplace
REF2=$HOME/workplace/Ovis_aries3.1
SOURCE=$HOME/bams
mkdir -p $HOME/EXP0000														##TBC
WASP=$HOME/wasp
DESTINATION=$HOME/EXP0000													##TBC

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/EXP0000_output										##TBC
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=$HOME/EXP0000/bams													##TBC
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/EXP0000_output											##TBC


#Loading module
module load R/3.4.3

mkdir -p ${OUTPUT}/GeneiASE_SVP/
#R prep for GeneiASE
R CMD BATCH --no-save $HOME/Rscripts/R_prep_static.R $HOME/logs/EXP0000_R_prep_static.Rout	##TBC

for gs in ${OUTPUT}/ASE/*/ASE_Count/*_ASERC.csv;do 
	ASENAME=$(basename -s _ASERC.csv $gs)
	mkdir -p ${OUTPUT}/GeneiASE_SVP/${ASENAME}/
	
done;
