#!/bin/bash

#SGE flags
#$ -N EXP0000_AN3													##TBC
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=10G
#$ -r yes
#$ -notify 
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 8
#$ -V

#Abs paths
REF=$HOME/workplace
REF2=$HOME/workplace/Ovis_aries3.1
SOURCE=$HOME/bams
mkdir -p $HOME/EXP0000														##TBC
WASP=$HOME/wasp
DESTINATION=$HOME/EXP0000													##TBC

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/EXP0000_output										##TBC
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=$HOME/EXP0000/bams													##TBC
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/EXP0000_output											##TBC

#Loading modules
module load fastqc/0.11.7
module load trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load samtools/1.6
module load picard/2.17.11
module load vcftools/0.1.13
module load bcftools/1.6
module load python/2.7.13


#Remapping loop (REF sorted and Indexed)
cd $OUTPUT

for b in ${OUTPUT}/WASP/*/*_rehead.bam;do
	BAMNAME=$(basename -s _rehead.bam $b)
	
	#Re-Mapping and alignment
	hisat2 -x ${REF2}/Oar_v3.1.92 \
	--known-splicesite-infile ${REF2}/Oar_v3.1.92_SpliceSites.txt \
	-p 8 \
	-t \
	--met 1 \
	--rna-strandness RF \
	--dta-cufflinks \
	-1 ${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_rehead.remap.fq1.gz \
	-2 ${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_rehead.remap.fq2.gz | samtools view -@ 8 -bS | samtools sort -@ 8 -o ${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped.bam

done;