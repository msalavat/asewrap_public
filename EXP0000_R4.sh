#!/bin/bash

#SGE flags
#$ -N EXP0000_R4						##TBC
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=30G
#$ -R yes
#$ -r yes
#$ -notify 
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V
#$ -t 1									##TBC The number of tasks should be set by the number of input BAM files in each run 


#Abs paths
REF=$HOME/workplace
REF2=$HOME/workplace/Ovis_aries3.1
SOURCE=$HOME/bams
mkdir -p $HOME/EXP0000														##TBC
WASP=$HOME/wasp
DESTINATION=$HOME/EXP0000													##TBC

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/EXP0000_output										##TBC
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=$HOME/EXP0000/bams													##TBC
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/EXP0000_output											##TBC

#Loading module
module load R/3.4.3

cd ${OUTPUT}/GeneiASE_SVP/output_icd_default/
ls -1 *_input_geneiase.txt > array.list_$SGE_TASK_ID
infile=$(awk "NR==$SGE_TASK_ID" array.list_$SGE_TASK_ID)

#GeneiASE for icd run
$HOME/tools/geneiase/bin/geneiase -t icd -b 1e4 -x 100 -l $HOME/tools/geneiase/lib/icd.lib.R -i ${OUTPUT}/GeneiASE_SVP/output_icd_default/${infile} -o ${OUTPUT}/GeneiASE_SVP/output_icd_default/${infile::-19}_output_geneiase.txt

rm -f array.list_*